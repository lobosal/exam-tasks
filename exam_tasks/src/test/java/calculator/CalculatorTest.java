package calculator;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    private Calculator c = new Calculator();

    @Test
    public void nullInputString(){
        assertNull(c.evaluate(null));
    }

    @Test
    public void emptyInputString(){
        assertNull(c.evaluate(""));
    }

    @Test
    public void inputStringContainsIllegalCharacters(){
        assertNull(c.evaluate("5*4.2-a1"));
    }

    @Test
    public void inputStringContainsIllegalOperations(){
        assertNull(c.evaluate("15.1/3.5+11-*14"));
    }

    @Test
    public void simpleInputString(){
        assertEquals(new Double(1.5), c.evaluate("1+3/6"));
    }

    @Test
    public void inputStringContainsSpaces(){
        assertEquals(new Double(3.7), c.evaluate("2*3 -5 + 5.4 / 2"));
    }

    @Test
    public void inputStringContainsParentheses(){
        assertEquals(new Double(3.5), c.evaluate("-5 + (1 + 15/2)  "));
    }
}
