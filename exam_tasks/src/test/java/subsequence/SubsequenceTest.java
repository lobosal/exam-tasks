package subsequence;

import org.junit.Test;
import java.util.*;

import static org.junit.Assert.*;

public class SubsequenceTest {

    private Subsequence s = new Subsequence();

    @Test
    public void listXLargerThanListY(){
        assertFalse(s.find(Arrays.asList(112, null, 99, "Forward", "B"), Arrays.asList(112, null, 99)));
    }

    @Test
    public void bothListsNull(){
        assertTrue(s.find(null, null));
    }

    @Test
    public void listXNullListYNotNull(){
        assertTrue(s.find(null, Arrays.asList(1, 14, "X")));
    }

    @Test
    public void listYContainsListX(){
        Set<Integer> set = new HashSet<>();

        assertTrue(s.find(Arrays.asList("art", set, 11), Arrays.asList("ssdsd", 11, "art", 4, set, 11, "stop")));
    }

    @Test
    public void listYDoesNotContainListX(){
        assertFalse(s.find(Arrays.asList(99.1, "sss", 1L), Arrays.asList(99.1, "sss", "a", 1)));
    }

    @Test
    public void emptyXList(){
        assertTrue(s.find(new ArrayList(), Arrays.asList("1", 12)));
    }

    @Test
    public void emptyYList(){
        assertFalse(s.find(Arrays.asList(11, 73276.22F), new ArrayList()));
    }
}
