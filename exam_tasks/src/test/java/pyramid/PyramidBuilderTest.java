package pyramid;

import org.junit.Test;
import java.util.*;

import static org.junit.Assert.assertArrayEquals;

public class PyramidBuilderTest {
    private PyramidBuilder pb = new PyramidBuilder();

    @Test(expected = CannotBuildPyramidException.class)
    public void nullList() throws CannotBuildPyramidException {
        pb.build(null);
    }

    @Test(expected = CannotBuildPyramidException.class)
    public void listContainsNulls() throws CannotBuildPyramidException {
        pb.build(Arrays.asList(1, 2, null));
    }

    @Test(expected = CannotBuildPyramidException.class)
    public void emptyList() throws CannotBuildPyramidException {
        pb.build(new ArrayList<>());
    }

    @Test(expected = CannotBuildPyramidException.class)
    public void listSizeLessThanThree() throws CannotBuildPyramidException {
        pb.build(Arrays.asList(1, 2));
    }

    @Test(expected = CannotBuildPyramidException.class)
    public void wrongListSize() throws CannotBuildPyramidException {
        pb.build(Arrays.asList(1, 2, 2, 4, 5));
    }

    @Test
    public void listOfThreeElements() throws CannotBuildPyramidException {
        int[][] actuals = pb.build(Arrays.asList(7, 2, 8));
        int[][] expecteds = new int[][] { {0, 2, 0}, {7, 0, 8} };

        for (int i = 0; i < expecteds.length; i++) {
            assertArrayEquals(expecteds[i], actuals[i]);
        }
    }

    @Test
    public void listOfSixElements() throws CannotBuildPyramidException {
        int[][] actuals = pb.build(Arrays.asList(1, 14, 2, 8, 3, 5));
        int[][] expecteds = new int[][] { {0, 0, 1, 0, 0}, {0, 2, 0, 3, 0}, {5, 0, 8, 0, 14} };

        for (int i = 0; i < expecteds.length; i++) {
            assertArrayEquals(expecteds[i], actuals[i]);
        }
    }
    
    @Test
    public void listOfTenElements() throws CannotBuildPyramidException {
        int[][] actuals = pb.build(Arrays.asList(7, 2, 8, 5, 1, 3, 4, 6, 9, 10));
        int[][] expecteds = new int[][] { {0, 0, 0, 1, 0, 0, 0}, {0, 0, 2, 0, 3, 0, 0}, {0, 4, 0, 5, 0, 6, 0},
                {7, 0, 8, 0, 9, 0, 10} };

        for (int i = 0; i < expecteds.length; i++) {
            assertArrayEquals(expecteds[i], actuals[i]);
        }
    }
}
