package pyramid;

import java.util.*;

class PyramidBuilder {

    private class ArraySize{
        private int rowCount = 2, columnCount = 3;
    }

    int[][] build(List<Integer> list) throws CannotBuildPyramidException {
        if (!checkListForNulls(list)){
            throw new CannotBuildPyramidException("Nulls aren't allowed");
        }

        ArraySize arraySize = getArraySize(list);
        int[][] result = new int[arraySize.rowCount][arraySize.columnCount];
        Collections.sort(list);
        int counter = 0;

        for (int i = 0; i < arraySize.rowCount; i++){
            for (int j = arraySize.columnCount / 2 - i; j <= arraySize.columnCount / 2 + i; j += 2) {
                result[i][j] = list.get(counter++);
            }
        }
        return result;
    }

    private  boolean checkListForNulls(List list){
        return list != null && !list.contains(null);
    }

    private ArraySize getArraySize(List list) throws CannotBuildPyramidException {
        ArraySize arraySize = new ArraySize();
        int counter = 3, increment = 3;

        while (counter < list.size()){
            counter += increment++;
            arraySize.rowCount++;
            arraySize.columnCount += 2;
        }

        if (counter != list.size()){
            throw new CannotBuildPyramidException("Illegal size of the input list");
        }

        return arraySize;
    }
}
