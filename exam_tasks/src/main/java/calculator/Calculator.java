package calculator;

import java.util.*;

class Calculator {

     Double evaluate(String input){
        if (input == null || input.length() == 0){
            return null;
        }
        try{
            return round(calculate(input));
        }
        catch (RuntimeException re){
            return null;
        }
    }

    private double calculate(String input){
        double result = 0.0;
        StringBuilder expression = new StringBuilder();//subExpressions consist of digits, spaces and '*' and '/' signs
        List<Character> signs = Arrays.asList('-', '+');
        Character currentSign = '=';
        StringBuilder subInput = new StringBuilder();//subInputs is an expression between parentheses

        for (int i = 0; i < input.length(); i++) {
            //extracting sub inputs
            if (input.charAt(i) == '('){
                int parenthesesCounter = 1;
                while(++i < input.length()){
                    char current = input.charAt(i);

                    if(current == '('){
                        parenthesesCounter++;
                    }
                    if(current == ')'){
                        parenthesesCounter--;
                    }
                    if(parenthesesCounter == 0){
                        break;
                    }
                    subInput.append(input.charAt(i));
                }

                if(i < input.length() - 1){
                    i++;
                }
            }
            //recursively replacing sub input with it's double value
            if(subInput.length() > 0){
                input = input.replace('(' + subInput.toString() + ')', String.valueOf(calculate(subInput.toString())));
                subInput = new StringBuilder();
            }
        }

        for (int i = 0; i < input.length(); i++) {
            char current = input.charAt(i);

            if(i + 1 == input.length() || signs.contains(current)){
                if(i == input.length() - 1){
                    expression.append(current);
                }
                boolean isExpressionEnded = true;
                //finding out if '-' is an operation sign or it is a sign of a negative double
                if(current == '-'){
                    isExpressionEnded = false;
                    for (int j = i - 1; j >= 0; j--) {
                        char checkedChar = input.charAt(j);
                        if(signs.contains(checkedChar) || checkedChar == '*' || checkedChar == '/' || checkedChar == '('){
                            break;
                        }
                        else{
                            if(input.charAt(j) != ' '){
                                isExpressionEnded = true;
                                break;
                            }
                        }
                    }
                }

                if(isExpressionEnded){
                    result = operate(currentSign, result, calculateExpression(expression.toString()));
                    expression = new StringBuilder();
                    currentSign = current;
                }
                else {
                    expression.append(current);
                }
            }
            else{
                expression.append(current);
            }
        }
        return result;
    }

    private double calculateExpression(String expression){
        double result = 0.0;
        StringBuilder currentDouble = new StringBuilder();
        char currentOperationSign = '=';
        List<Character> operationSigns = Arrays.asList('*', '/');

        for (int i = 0; i < expression.length(); i++) {
            char temp = expression.charAt(i);

            if (temp != ' ' && !operationSigns.contains(temp)){
                currentDouble.append(temp);
            }
            if (i + 1 == expression.length() || operationSigns.contains(temp)){
                result = operate(currentOperationSign, result, new Double(currentDouble.toString()));
                currentOperationSign = temp;
                currentDouble = new StringBuilder();
            }
        }
        return result;
    }

    private double operate(char operationSign, double d1, double d2){
        switch (operationSign){
            case '=':
                d1 = d2;
                break;
            case '+':
                d1 += d2;
                break;
            case '-':
                d1 -= d2;
                break;
            case '*':
                d1 *= d2;
                break;
            case '/':
                d1 /= d2;
                break;
            default:
                break;
        }
        return d1;
    }

    private double round(Double d){
        String number = d.toString();
        int index = number.indexOf('.');
        //rounding if there's more than 4 characters after '.'
        if (index != -1 && index + 5 < number.length()){
            int temp = Integer.valueOf(number.substring(index + 5, index + 6));
            //taking substring due to the value of the 5th character after '.'
            number = temp >= 5 ? number.substring(0, index + 4) + (Integer.valueOf(number.substring(index + 4,
                    index + 5)) + 1) : number.substring(0, index + 5);
        }
        return new Double(number);
    }
}
