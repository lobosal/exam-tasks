package subsequence;

import java.util.List;

class Subsequence {

    boolean find(List x, List y){
        if (x == null || x.size() == 0){
            return true;
        }

        if (y == null){
            return false;
        }

        int counter = 0;

        for (int i = 0; i < y.size(); i++) {
            if (y.size() - i < x.size() - counter){
                return false;
            }

            if (areEqual(y.get(i), x.get(counter))){
                counter++;
            }

            if (counter == x.size()){
                return true;
            }
        }
        return false;
    }

    private boolean areEqual(Object o1, Object o2){
        return o1 == null ? o2 == null : o1.equals(o2);
    }
}
